require 'deploy/utility'

module Deploy
  module Checks
    include Deploy::Utility

    def check_setup
      (shout('docker not installed'); exit(1)) unless command?('docker')
      (shout('can\'t find Deployfile'); exit(1)) unless File.readable?('Deployfile')
      (shout('AWS credentials not configured.'); exit(1)) unless ENV.fetch('AWS_ACCESS_KEY_ID', nil) && ENV.fetch('AWS_SECRET_ACCESS_KEY', nil) && ENV.fetch('AWS_REGION', nil)
    end

    def check_rollback_version(version, environment)
      check_version(version, environment)
      (shout('You can only rollback to a previous version'); exit(1)) unless application_versions_array.include?(version)
    end

    def check_version(version, environment)
      (shout('You must pass a version with -v'); exit(1)) unless version
      (shout('You are currently on that version'); exit(1)) if current_version_for_environment(environment) == version
    end

  end
end
