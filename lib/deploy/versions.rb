require 'deploy/utility'

module Deploy
  module Versions
    include Deploy::Utility

    def ecr
      @ecr ||= Aws::ECR::Client.new
    end

    def version_exists?(service, version)
      @array ||= ecr.list_images(repository_name: service).image_ids.reverse.map(&:image_tag).compact
      @array.include?(version)
    end

  end
end
