require 'deploy/utility'
require 'zip'

module Deploy
  module Commands
    include Deploy::Utility

    def ecs
      @ecs ||= Aws::ECS::Client.new
    end

    def alb
      @alb ||= Aws::ElasticLoadBalancingV2::Client.new
    end

    def ecr_login
      shout "Logging in to AWS ECR"
      command = "eval $(aws ecr get-login --no-include-email)"
      exit(1) unless system(command)
    end

    def build_image(repo, tag)
      shout "Building Docker Image: #{repo}:#{tag}"
      command = "docker build -t #{repo}:#{tag} ."
      exit(1) unless system(command)
    end

    def push_image(repo, tag)
      ecr_login
      shout "Pushing Docker Image: #{repo}:#{tag}"
      command = "docker push #{repo}:#{tag}"
      exit(1) unless system(command)
    end

    def pull_image(repo, tag)
      ecr_login
      shout "Pulling Docker Image: #{repo}:#{tag}"
      command = "docker pull #{repo}:#{tag}"
      exit(1) unless system(command)
    end

    def run_deploy(cluster, service, version, environment, repo)
      configs = Deploy::Config[environment]['configs']

      defs = configs.map { |config| build_def(config, service, version, environment) }

      response = ecs.register_task_definition({
        container_definitions: defs,
        volumes: Deploy::Config[environment]['volumes'],
        family: "#{service}-#{environment}"
      })

      arn = response.task_definition.task_definition_arn
      exit(1) unless arn


      begin

        exit(1) unless ecs.update_service(cluster: cluster, service: service, desired_count: Deploy::Config[environment]['count'], task_definition: arn)

      rescue Aws::ECS::Errors::ServiceNotFoundException, Aws::ECS::Errors::ServiceNotActiveException, Aws::ECS::Errors::InvalidParameterException => e
        shout "Caught an exception. Here is the message: #{e.message}"
        shout "Couldn't find a service with the name '#{service}'. Creating one for you."

        service_hash = {
          cluster: cluster,
          service_name: service,
          task_definition: arn,
          desired_count: Deploy::Config[environment]['count']
        }

        tg = target_group(cluster, service)
        shout 'Here is the target group that we found:'
        shout tg.inspect

        if tg
          shout "Looks like we found a target group with the name '#{cluster}-#{service}'. We are adding a load balancer to this service."

          service_hash.merge!({
            role: Deploy::Config[environment]['role'],
            load_balancers: [
              {
                container_name: Deploy::Config[environment]['lb_container'],
                container_port: Deploy::Config[environment]['lb_port'],
                target_group_arn: tg.target_group_arn
              }
            ]
          })
        end

        shout 'We are creating your service with this hash:'
        shout service_hash.to_s

        exit(1) unless ecs.create_service(service_hash)

      end
    end

    def build_def(config, service, version, environment)
      definition = {
        name: config['name'],
        cpu: config['cpu'],
        essential: config['essential'],
        image: "#{config['image']}:#{config['version'] ? config['version'] : version}",
        memory: config['memory'],
        log_configuration: {
          log_driver: 'awslogs',
          options: {
            'awslogs-group' => "#{service}-#{environment}",
            'awslogs-region' => 'us-west-2', # TODO
            'awslogs-stream-prefix' => "#{service}-#{environment}"
          }
        }
      }

      definition['port_mappings'] = config['port_mappings'] if config['port_mappings']
      definition['volumes_from'] = config['volumes_from'] if config['volumes_from']
      definition['mount_points'] = config['mount_points'] if config['mount_points']
      definition['links'] = config['links'] if config['links']
      definition['environment'] = config['environment'].map { |name, value| { name: name, value: value } } if config['environment']

      definition
    end

    def target_group(cluster, service)
      alb.describe_target_groups.target_groups.select { |tg| tg.target_group_name == "#{cluster}-#{service}" }.first
    end

    def run_rollback(version, environment)
      command = "eb deploy #{environment} --version #{version} --timeout 60"
      shout "deploying #{version} to elastic beanstalk with command:\n\t#{command}"
      exit(1) unless system(command)
    end

  end
end
