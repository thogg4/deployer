module Deploy
  class Config

    def self.[](env)
      self.config[env]
    end

    def self.config
      @config ||= YAML.load_file('Deployfile')
    end

    def self.require_file
      file = self.config['require_this']
      return true unless file
      require file
    end

    self.require_file
  end
end
