module Deploy
  class Runner < Thor
    include Deploy::Output
    include Deploy::Commands
    include Deploy::Checks
    include Deploy::Versions
    include Deploy::Utility

    method_option :version, aliases: '-v', desc: 'Version', type: :string, required: true, default: Time.now.strftime('%Y-%m-%d-%I-%M')
    method_option :environment, aliases: '-e', desc: 'Environment Name', type: :string, required: true, default: 'stage'
    method_option :build, aliases: '-b', desc: 'Build Image', type: :boolean, default: true
    desc 'deploy application', 'build image and deploy to ecs'
    def deploy
      environment = options[:environment]
      cluster = Deploy::Config[environment]['cluster']
      service = Deploy::Config[environment]['service']
      repo = Deploy::Config[environment]['repo']

      version = options[:version]
      build = options[:build]

      if build && !version_exists?(service, version)
        announce({ color: '#6080C0', title: "#{service} deployment to #{environment} started", text: "cluster: #{cluster} \n service: #{service} \n version: #{version}" })
        build_image(repo, version)
        push_image(repo, version)
      else
        announce({ color: '#6080C0', title: "#{service} deployment to #{environment} started", text: "cluster: #{cluster} \n service: #{service} \n version: #{version}" })
      end

      run_deploy(cluster, service, version, environment, repo)
      announce({ color: 'good', title: "#{service} deployment to #{environment} has succeeded!!", text: "cluster: #{cluster} \n service: #{service} \n current version: #{version}" })
    end

    desc 'test_slack', 'send test notification'
    def test_slack
      announce({ color: 'good', title: 'This is a test notification from deployer', text: "Text section \n Second Line" })
    end

    desc 'versions', 'list all application versions'
    def versions
      check_setup

      images_array.each do |image|
        shout image
      end
    end

    method_option :service, aliases: '-s', desc: 'Service', required: true
    desc 'version', 'show environment version'
    def version
      check_setup

      # TODO
      #shout current_version_for_service(options[:service])
    end

  end
end
