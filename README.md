# Deployer

## Installation

gem install deployer

## Usage

### Dependencies

* ruby
* aws cli
* docker

### Deployfile

Each project must have a Deployfile.

Example:

```
require_this: app_file_to_require

environment_name:
  cluster: ecs_cluster_name
  service: service_name
  repo: docker_repo
  count: 1
  role: ecs_service_role
  lb_port: 8080
  lb_container: container_that_should_be_load_balanced
  configs:
  - name: project_name
    image: docker_image
    memory: 2000
    cpu: 512
    essential: true
    port_mappings:
      - container_port: 8080
        host_port: 0
        protocol: tcp
    environment:
      ENV_VAR: 'value'

```
